$(function () {
    var $console = $("#console");
    var $btn01 = $("#btn01");
    var count = 0;

    $btn01.off('click').on('click', function () {
        count++;
        $console.text("Count : " + count);
        console.log("Count : " + count);
    });
    
    // jQuery 最新的事件綁定方式
    // off 解除綁定
    // on 綁定事件
    //other : click , bind
});


//$(document).ready(function () {
//    var $console = $("#console");
//    var $btn01 = $("#btn01");
//    var count = 0;
//
//    $btn01.off('click').on('click', function () {
//        count++;
//        $console.text("Count : " + count);
//        console.log("Count : " + count);
//    });
//});
