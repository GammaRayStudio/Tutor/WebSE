$(function () {
    // Apply
    var applyCount = 0;
    
    // Query
    var queryCount = 0;
    
    
    var initPageView = function () {
        initApplyView();
    }

    // ApplyDiv
    var initApplyView = function () {
        var view = initApplyViewEvent();
        var ajax = initApplyDataAjax();
        view.setBtnApplyEvent(ajax);
        view.show();
    }

    var initApplyViewEvent = function () {
        var $divApply = $("#applyDiv");
        var $titleApply = $divApply.find("#titleApply");
        var $textApply = $divApply.find("#textApply");
        var $btnApply = $divApply.find("#btnApply");
        var $btnNext = $divApply.find("#btnNext");

        var view = {
            show : function(){
                $divApply.show();
            },
            hide : function(){
                $divApply.hide();
            },
            setTitleApply : function(msg){
                $titleApply.text(msg);
            },
            setTextApply : function(msg){
                $textApply.text(msg)
            },
            setBtnApplyEvent : function(ajax){
                $btnApply.off("click").on("click" , function(){  
                    ajax.get(view.setTextApply);
                })
            },
            setBtnNextEvent : function(){
                $btnNext.off("click").on("click" , function(){
                    $divApply.hide();
                    initQueryView();
                })
            }
        }
        view.setBtnNextEvent();
        return view;
    }

    var initApplyDataAjax = function () {
        var ajax = {
            get: function (setTextApply) {
                // do something about ajax
                applyCount ++;
                var text = "OwO / Ajax Data (" + applyCount + ")";
                setTextApply(text);
            }
        }
        return ajax;
    }

    // QueryDiv
    var initQueryView = function () {
        var view = initQueryViewEvent();
        var ajax = initQueryDataAjax();
        view.setBtnQueryEvent(ajax);
        view.show();
    }

    var initQueryViewEvent = function () {
        var $queryDiv = $("#queryDiv");
        var $titleQuery = $queryDiv.find("#titleQuery");
        var $textQuery = $queryDiv.find("#textQuery");
        var $btnQuery = $queryDiv.find("#btnQuery");
        var $btnPrev = $queryDiv.find("#btnPrev");
        
        var view = {
            show : function(){
                $queryDiv.show();
            },
            hide : function(){
                $queryDiv.hide();
            },
            setTitleQuery : function(msg){
                $titleQuery.text(msg);
            },
            setTextQuery : function(msg){
                $textQuery.text(msg)
            },
            setBtnQueryEvent : function(ajax){
                $btnQuery.off("click").on("click" , function(){  
                    ajax.get(view.setTextQuery);
                })
            },
            setBtnPrevEvent : function(){
                $btnPrev.off("click").on("click" , function(){
                    $queryDiv.hide();
                    initApplyView();
                })
            }
        }
        view.setBtnPrevEvent();
        
        $queryDiv.css("background" , "yellow")
        
        return view;
    }

    var initQueryDataAjax = function () {
        var ajax = {
            get: function (setTextQuery) {
                // do something about ajax
                $.ajax({
                    url: "api/AjaxData.json",
                    type: "GET",
                    data: {
                        id : 1
                    },
                    success: function(result) {
                        for (var key in result) {
                            console.log("key : " + key + " , value : " + result[key] + "<br>")
                        }
                        setTextQuery("id : " + result.id + " , name : " + result.name)
                    }
                });
            }
        }
        return ajax;
    }

    // Cancel
    var initCancelView = function () {

    }

    var initCancelViewEvent = function () {

    }

    var initCancelDataAjax = function () {

    }

    initPageView();
})
